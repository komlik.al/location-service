package by.cnad.location.rest;

import by.cnad.location.service.LocationDto;
import by.cnad.location.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/locations")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LocationController {

  private final LocationService service;

  @Value("${location.version}")
  private String version;

  @RequestMapping(value = "/hello", method = RequestMethod.GET)
  public String sayHello() {
    return "Hello from location service version: " + version;
  }


  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public LocationDto getLocation(@PathVariable Long id) {
    return service.getCompanyById(id);
  }



}
