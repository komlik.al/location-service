package by.cnad.location.service;

import by.cnad.location.repo.LocationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LocationService {

  private final LocationRepository repository;
  private final LocationMapper mapper;

  public LocationDto getCompanyById(Long id) {
    return repository.findById(id)
        .map(mapper::toDto)
        .orElseThrow(()-> new ResourceNotFoundException("Location not found"));
  }
}
